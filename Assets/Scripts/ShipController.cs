﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour {

	public GameObject holder;

	public const float FORWARD_FORCE = 3;
	public const float STRAFE_HORIZONTAL_FORCE = 1;
	public const float STRAFE_VERTICAL_FORCE = 1;
	public const float BARREL_ROLL_FORCE = 1;
	public const float YAW_FORCE = 1;
	public const float PITCH_FORCE = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.W)) {
			holder.GetComponent<Rigidbody> ().AddForce (holder.transform.up * STRAFE_VERTICAL_FORCE);
		}
		if (Input.GetKey (KeyCode.S)) {
			holder.GetComponent<Rigidbody> ().AddForce (-holder.transform.up * STRAFE_VERTICAL_FORCE);
		}
		if (Input.GetKey (KeyCode.D)) {
			holder.GetComponent<Rigidbody> ().AddForce (holder.transform.right * STRAFE_HORIZONTAL_FORCE);
		}
		if (Input.GetKey (KeyCode.A)) {
			holder.GetComponent<Rigidbody> ().AddForce (-holder.transform.right * STRAFE_HORIZONTAL_FORCE);
		}
		if (Input.GetKey (KeyCode.R)) {
			holder.GetComponent<Rigidbody> ().AddForce (holder.transform.forward * FORWARD_FORCE);
		}
		if (Input.GetKey (KeyCode.F)) {
			holder.GetComponent<Rigidbody> ().AddForce (-holder.transform.forward * FORWARD_FORCE);
		}
		if (Input.GetKey (KeyCode.Q)) {
			holder.GetComponent<Rigidbody> ().AddRelativeTorque (0, 0, BARREL_ROLL_FORCE);
		}
		if (Input.GetKey (KeyCode.E)) {
			holder.GetComponent<Rigidbody> ().AddRelativeTorque (0, 0, -BARREL_ROLL_FORCE);
		}
		if (Input.GetKey (KeyCode.UpArrow)) {
			holder.GetComponent<Rigidbody> ().AddRelativeTorque (-PITCH_FORCE, 0, 0);
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			holder.GetComponent<Rigidbody> ().AddRelativeTorque (PITCH_FORCE, 0, 0);
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			holder.GetComponent<Rigidbody> ().AddRelativeTorque (0, -YAW_FORCE, 0);
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			holder.GetComponent<Rigidbody> ().AddRelativeTorque (0, YAW_FORCE, 0);
		}
	}
}
