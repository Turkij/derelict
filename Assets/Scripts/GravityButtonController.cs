﻿using UnityEngine;
using System.Collections;

public class GravityButtonController : MonoBehaviour, Interactor{

	public void Interact(GameObject player){
		if (Vector3.Distance (transform.position, player.transform.position) < 3f) {
			GameController.GC.ToggleGravity ();
		}
	}
}
