﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	public static GameController GC;
	public Camera inv_cam;
	public bool gravity = true;
	public List<GameObject> inventory;
	public int selected = -1;
	public GameObject selector;
	public GameObject stars;

	public const float NO_GRAVITY_NUDGE_FORCE = 1.1f;

	// Use this for initialization
	void Start () {
		GC = this;
		UpdateSelected ();
	}
	
	// Update is called once per frame
	void Update () {
		if (stars) {
			stars.transform.Rotate(new Vector3(0, .1f, 0));
		}
	}

	public void SetGravity(bool newGravity){
		if (gravity != newGravity){
			ToggleGravity();
		}
	}

	public void ToggleGravity(){
		gravity = !gravity;

		List<Rigidbody> rigidBodies = new List<Rigidbody> ();
		foreach (GameObject go in UnityEngine.Object.FindObjectsOfType<GameObject>()){
			if (go.activeInHierarchy){
				Rigidbody rb = go.GetComponent<Rigidbody>();
				if(rb != null){
					rigidBodies.Add (rb);
				}
			}
		}
		foreach(Rigidbody rb in rigidBodies){
			if (gravity) {
				rb.useGravity = true;
			} else {
				rb.useGravity = false;
				rb.AddForce(rb.mass * NO_GRAVITY_NUDGE_FORCE*Vector3.up);
			}
		}
	}

	public void Pickup(GameObject obj){
		obj.layer = 8;
		inventory.Add (obj);
		for (int i=0; i<inventory.Count; i++) {
			inventory[i].transform.position = inv_cam.ScreenToWorldPoint(new Vector3(inv_cam.pixelWidth-70-70*i, 40, 1) );
		}
		if (obj.GetComponent<Rigidbody> ()) {
			obj.GetComponent<Rigidbody> ().isKinematic = true;
		}
		if (selected == -1) {
			selected = 0;
			UpdateSelected();
		}
	}

	public void Drop(GameObject obj){
		if (inventory.Remove (obj)) {
			obj.layer = 0;
			for (int i=0; i<inventory.Count; i++) {
				inventory [i].transform.position = new Vector3 (2 - inventory.Count * 1.6f, 1, 0);
			}
			if (obj.GetComponent<Rigidbody> ()) {
				obj.GetComponent<Rigidbody> ().isKinematic = false;
			}
			if (selected == inventory.Count) {
				selected = -1;
				UpdateSelected ();
			}
		}
	}

	public void CycleLeft(){
		selected = inventory.Count == 0 ? -1 : (selected + 1) % inventory.Count;
		UpdateSelected ();
	}

	public void CycleRight(){
		selected = inventory.Count == 0 ? -1 : (selected - 1) % inventory.Count;
		UpdateSelected ();
	}

	public void UpdateSelected(){
		if (selected == -1) {
			selector.SetActive(false);
		}else{
			selector.SetActive(true);
			selector.GetComponent<RectTransform>().localPosition = new Vector3(inv_cam.pixelWidth/2-70-70*selected, -184, 0);
		}
	}
}
