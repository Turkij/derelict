﻿using UnityEngine;
using System.Collections;

public class ElevatorButtonController : MonoBehaviour, Interactor{

	public GameObject elevator;
	public GameObject fade;
	public GameObject  player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Interact(GameObject player){
		if (Vector3.Distance (transform.position, player.transform.position) < 3f) {
			StartCoroutine (ElevatorUp ());
			StartCoroutine (LoadLevelAfterDelay (5));
		}
	}

	private IEnumerator ElevatorUp(){
		player.transform.parent = transform;
		while (true) {
			elevator.transform.position += new Vector3(0, 1, 0);
			yield return null;
		}
	}

	private IEnumerator Fadeout(int frames){
		yield return null;
	}

	private IEnumerator LoadLevelAfterDelay(int delay){
		yield return new WaitForSeconds(delay);
		Application.LoadLevel ("Scene 1");
	}
}
