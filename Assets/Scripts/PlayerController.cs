﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		/*if(Input.GetKeyDown(KeyCode.Q)){
			GameController.GC.ToggleGravity();
		}*/
		if(Input.GetKeyDown(KeyCode.E)){
			GameObject go = GetClosestInteractor();
			go.GetComponent<Interactor>().Interact(gameObject);
		}
		if(Input.GetKeyDown(KeyCode.F)){
			GameController.GC.CycleLeft();
		}
		/*if(Input.GetKeyDown(KeyCode.G)){
			if(GameController.GC.selected != -1){
				GameObject go = GameController.GC.inventory[GameController.GC.selected];
				GameController.GC.Drop (go);
				go.transform.position = transform.position;
			}
		}*/
	}

	GameObject GetClosestInteractor(){
		GameObject closest = null;
		float shortestDist = float.MaxValue;
		foreach (GameObject current in GameObject.FindGameObjectsWithTag("Interactor")) {
			float dist = Vector3.Distance(transform.position, current.transform.position);
			if(dist < shortestDist){
				shortestDist = dist;
				closest = current;
			}
		}
		return closest;
	}
}
