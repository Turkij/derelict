﻿using UnityEngine;
using System.Collections;

public interface Interactor {
	void Interact(GameObject player);
}
