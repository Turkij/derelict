﻿using UnityEngine;
using System.Collections;

public class ShipRefuelable : MonoBehaviour, Interactor{

	public bool fueled = false;

	public void Interact(GameObject player){
		if (Vector3.Distance (transform.position, player.transform.position) < 4.5f) {
			if (fueled) {
				Application.LoadLevel ("space");
			}
		}
	}

	public void OnCollisionEnter(Collision col){
		if (col.gameObject.tag == "Fueltank") {
			Destroy(col.gameObject);
			fueled = true;
		}
	}
}
