﻿using UnityEngine;
using System.Collections;

public class DoorController : MonoBehaviour, Interactor {

	private bool open = false;
	public Vector3 openDist = 1.5f*Vector3.right;

	public bool isOpen{
		get{return open;}
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ToggleOpen(){
		open = !open;
		StartCoroutine(DoMove(!open, 30));
	}

	public void Interact(GameObject player){
		if (Vector3.Distance (transform.position, player.transform.position) < 3f){
			ToggleOpen ();
		}
	}

	public IEnumerator DoMove(bool open, int frames){
		Vector3 increment = openDist / frames;
		if (!open) {
			increment *= -1;
		}
		for (int i=0; i<frames; i++) {
			transform.position += increment;
			yield return null;
		}
	}
}
