﻿using UnityEngine;
using System.Collections;

public class Pickupable : MonoBehaviour, Interactor{

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Interact(GameObject player){
		if (Vector3.Distance (transform.position, player.transform.position) < 3f) {
			GameController.GC.Pickup (gameObject);
		}
	}
}
